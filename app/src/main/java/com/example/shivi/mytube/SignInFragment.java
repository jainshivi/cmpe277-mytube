package com.example.shivi.mytube;

import android.app.Activity;
import android.app.Fragment;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.android.gms.common.SignInButton;

/**
 * A placeholder fragment containing a simple view.
 */
public class SignInFragment extends Fragment implements View.OnClickListener {
    View rootView;
    Activity mainActivity;
    TextView mStatus;

    public SignInFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_sign_in, container, false);
        SignInButton button = (SignInButton) rootView.findViewById(R.id.sign_in_button);
        mStatus = (TextView) rootView.findViewById(R.id.mStatus);
        button.setOnClickListener(this);
        return rootView;
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.sign_in_button) {
            ((MainActivity)mainActivity).onSignInClicked();
            mStatus.setText(R.string.signing_in);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mainActivity = activity;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
