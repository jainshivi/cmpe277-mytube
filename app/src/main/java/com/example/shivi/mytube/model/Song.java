package com.example.shivi.mytube.model;

import android.graphics.Bitmap;

public class Song {
    private String id;
    private String playListId;
    private Bitmap thumbnail;
    private String name;
    private String publishedDate;
    private String thumbnailUrl;
    private String numberOfViews;

    public Song(String id, Bitmap thumb, String thumbnailUrl, String name, String publishedDate, String numberOfViews) {
        this.id = id;
        this.thumbnail = thumb;
        this.thumbnailUrl = thumbnailUrl;
        this.name = name;
        this.publishedDate = publishedDate;
        this.numberOfViews = numberOfViews;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayListId() {
        return playListId;
    }

    public void setPlayListId(String playListId) {
        this.playListId = playListId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public Bitmap getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Bitmap thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getNumberOfViews() {
        return numberOfViews;
    }

    public void setNumberOfViews(String numberOfViews) {
        this.numberOfViews = numberOfViews;
    }

}
