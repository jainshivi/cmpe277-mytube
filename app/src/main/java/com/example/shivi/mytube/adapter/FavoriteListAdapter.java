package com.example.shivi.mytube.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shivi.mytube.MyTubeHomeActivity;
import com.example.shivi.mytube.R;
import com.example.shivi.mytube.model.Song;
import com.example.shivi.mytube.youtube.PlaylistManager;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import com.squareup.picasso.Picasso;

public class FavoriteListAdapter extends BaseAdapter {
    private MyTubeHomeActivity activity;
    private LayoutInflater inflater;
    private List<Song> songItems;
    private ImageButton favoriteButton;
    private GoogleAccountCredential credential;
    private PlaylistManager playlistManager;

    private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();

    public FavoriteListAdapter(MyTubeHomeActivity activity, List<Song> songItems, GoogleAccountCredential credential) {
        this.activity = activity;
        this.songItems = songItems;
        this.credential = credential;
    }

    @Override
    public int getCount() {
        return songItems.size();
    }

    @Override
    public Object getItem(int position) {
        return songItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        FavoriteViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.favorite_list_row, null);
            holder = new FavoriteViewHolder();
            final Song song = songItems.get(position);
            holder.song = song;
            holder.nameView = (TextView) convertView.findViewById(R.id.fav_title);
            holder.noOfViewsView = (TextView) convertView.findViewById(R.id.fav_numberOfViews);
            holder.publishedDateView = (TextView) convertView.findViewById(R.id.fav_publishedDate);
            holder.imageView = (ImageView) convertView.findViewById(R.id.fav_thumbnail);
            convertView.setTag(holder);
        } else {
            holder = (FavoriteViewHolder) convertView.getTag();
        }

        final Song song = songItems.get(position);

        holder.nameView.setText(song.getName());
        holder.noOfViewsView.setText("Views: " + String.valueOf(song.getNumberOfViews()));
        holder.publishedDateView.setText("Published date: " + song.getPublishedDate());

        // Set thumbnail view
        if (holder.imageView != null) {
            Picasso.with(activity)
                    .load(song.getThumbnailUrl())
                    .placeholder(R.drawable.default_placeholder)
                    .into(holder.imageView);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.playVideo(song.getId(), "Favorite");
            }
        });

        convertView.setBackgroundColor(convertView.getResources().getColor(R.color.list_row_hover_end_color));

        if (mSelection.get(position) != null) {
            convertView.setBackgroundColor(convertView.getResources().getColor(R.color.list_row_hover_start_color));
        }

        return convertView;
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public List<String> getItemsToRemove(){
        List<String> itemsToRemove = new ArrayList<String>();
        for(Integer currentPosition : mSelection.keySet()) {
            Song song = songItems.get(currentPosition);
            itemsToRemove.add(song.getId());
            Log.d("Removed", "Id:" + song.getId() + " , Title:" + song.getName());
        }
        return itemsToRemove;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }
}



