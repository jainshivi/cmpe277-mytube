package com.example.shivi.mytube.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.example.shivi.mytube.model.Song;

public class FavoriteViewHolder {
    TextView noOfViewsView;
    TextView nameView;
    TextView publishedDateView;
    ImageView imageView;
    public Song song;
}
