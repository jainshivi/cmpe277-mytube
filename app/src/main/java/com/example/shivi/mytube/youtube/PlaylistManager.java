package com.example.shivi.mytube.youtube;

import com.example.shivi.mytube.MyTubeHomeActivity;
import com.example.shivi.mytube.model.Song;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.Joiner;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.*;
import java.io.IOException;
import java.util.*;
import android.os.AsyncTask;
import android.util.Log;


public class PlaylistManager extends AsyncTask<String, String, ArrayList<Song>>{

    private YouTube youtube;
    private GoogleAccountCredential credential;
    private MyTubeHomeActivity activity;

    public PlaylistManager(MyTubeHomeActivity activity, GoogleAccountCredential credential)
    {   this.activity = activity;
        this.credential = credential;
    }

    @Override
    public ArrayList<Song> doInBackground(String... params) {
        ArrayList<Song> songs = null;
        String playlistId = params[0], videoId = params[1], operation = params[2];
        try {
            youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
                    .setApplicationName("MyTube").build();

            if(playlistId == null) {
                playlistId = createNewPlaylist();
                this.activity.setPlaylistId(playlistId);
            }

            if(operation.equals("Insert")) {
                insertVideoToPlaylist(playlistId, videoId);
                songs = getPlaylistItems(playlistId);
            }
            if(operation.equals("Remove")) {
                removeVideoFromPlaylist(playlistId, videoId);
                songs = getPlaylistItems(playlistId);
            }
            if(operation.equals("List"))
                songs = getPlaylistItems(playlistId);

        } catch (GoogleJsonResponseException e) {
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : " + e.getDetails().getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("IOException: " + e.getMessage());
            e.printStackTrace();
        } catch (Throwable t) {
            System.err.println("Throwable: " + t.getMessage());
            t.printStackTrace();
        }
        return songs;
    }

    @Override
    protected void onPostExecute(ArrayList<Song> songs) {
        if(songs != null)
            activity.renderFavoriteList(songs);
    }

    /**
     * Create a playlist and add it to the authorized account.
     */
    private String createNewPlaylist() throws IOException {

        YouTube.Playlists.List  searchList = youtube.playlists().list("id,snippet,contentDetails");
        searchList.setFields("etag,eventId,items(contentDetails,etag,id,kind,player,snippet,status),kind,nextPageToken,pageInfo,prevPageToken,tokenPagination");
        searchList.setMine(true);
        searchList.setMaxResults((long) 5);
        PlaylistListResponse playListResponse = searchList.execute();
        List<Playlist> playlists = playListResponse.getItems();

        if (playlists != null) {
            Iterator<Playlist> iteratorPlaylistResults = playlists.iterator();
            if (!iteratorPlaylistResults.hasNext()) {
                System.out.println(" There aren't any results for your query.");
            }
            while (iteratorPlaylistResults.hasNext()) {
                Playlist playlist = iteratorPlaylistResults.next();
                if(playlist.getSnippet().getTitle().equals("SJSU-CMPE-277")){
                    Log.d("FetchPlaylistId : ",  playlist.getId() + playlist.getSnippet().getTitle());
                    return playlist.getId();
                }
            }
        }

        // This code constructs the playlist resource that is being inserted.
        // It defines the playlist's title, description, and privacy status.
        PlaylistSnippet playlistSnippet = new PlaylistSnippet();
        playlistSnippet.setTitle("SJSU-CMPE-277");
        playlistSnippet.setDescription("A playlist created with the YouTube API v3");
        PlaylistStatus playlistStatus = new PlaylistStatus();
        playlistStatus.setPrivacyStatus("public");

        Playlist youTubePlaylist = new Playlist();
        youTubePlaylist.setSnippet(playlistSnippet);
        youTubePlaylist.setStatus(playlistStatus);

        // Call the API to insert the new playlist. In the API call, the first argument identifies the resource parts
        // that the API response should contain, and the second argument is the playlist being inserted.
        YouTube.Playlists.Insert playlistInsertCommand = youtube.playlists().insert("snippet,status", youTubePlaylist);
        playlistInsertCommand.setKey(Config.YOUTUBE_API_KEY);

        Playlist playlistInserted = playlistInsertCommand.execute();

        // Print data from the API response and return the new playlist's unique playlist ID.
        Log.d("InsertNewPlaylist", "New Playlist name: " + playlistInserted.getSnippet().getTitle());
        Log.d("InsertNewPlaylist", " - Privacy: " + playlistInserted.getStatus().getPrivacyStatus());
        Log.d("InsertNewPlaylist", " - Description: " + playlistInserted.getSnippet().getDescription());
        Log.d("InsertNewPlaylist", " - Posted: " + playlistInserted.getSnippet().getPublishedAt());
        Log.d("InsertNewPlaylist", " - Channel: " + playlistInserted.getSnippet().getChannelId() + "\n");

        return playlistInserted.getId();
    }

    /**
     * Create a playlist item with the specified video ID and add it to the specified playlist.
     * @param playlistId assign to newly created playlistitem
     * @param videoId    YouTube video id to add to playlistitem
     */
    private String insertVideoToPlaylist(String playlistId, String videoId) throws IOException {

        // Define a resourceId that identifies the video being added to the playlist.
        ResourceId resourceId = new ResourceId();
        resourceId.setKind("youtube#video");
        resourceId.setVideoId(videoId);

        // Set fields included in the playlistItem resource's "snippet" part.
        PlaylistItemSnippet playlistItemSnippet = new PlaylistItemSnippet();
        playlistItemSnippet.setTitle("First video in the test playlist");
        playlistItemSnippet.setPlaylistId(playlistId);
        playlistItemSnippet.setResourceId(resourceId);

        // Create the playlistItem resource and set its snippet to the object created above.
        PlaylistItem playlistItem = new PlaylistItem();
        playlistItem.setSnippet(playlistItemSnippet);

        // In the API call, the first argument identifies the resource parts that the API response should contain,
        // and the second argument is the playlist item being inserted.
        YouTube.PlaylistItems.Insert playlistItemsInsertCommand =
                youtube.playlistItems().insert("snippet,contentDetails", playlistItem);
        PlaylistItem returnedPlaylistItem = playlistItemsInsertCommand.execute();

        // Print data from the API response and return the new playlist item's unique playlistItem ID.
        Log.d("InsertVideoToPlaylist", "New PlaylistItem name: " + returnedPlaylistItem.getSnippet().getTitle());
        Log.d("InsertVideoToPlaylist", " - Video id: " + returnedPlaylistItem.getSnippet().getResourceId().getVideoId());
        Log.d("InsertVideoToPlaylist", " - Posted: " + returnedPlaylistItem.getSnippet().getPublishedAt());
        Log.d("InsertVideoToPlaylist", " - Channel: " + returnedPlaylistItem.getSnippet().getChannelId());

        this.activity.setFavoritPlayListItemId(videoId, returnedPlaylistItem.getId());
        return returnedPlaylistItem.getId();
    }

    private void removeVideoFromPlaylist(String playlistId, String videoId) throws IOException{
        String playlistItemId = this.activity.getFavoritPlayListItemId(videoId);

        YouTube.PlaylistItems.Delete playlistItemsDeleteCommand =
                youtube.playlistItems().delete(playlistItemId);
        playlistItemsDeleteCommand.execute();

        this.activity.removeFavoritPlayListItemId(videoId);
    }

    private ArrayList<Song> getPlaylistItems(String playlistId) {
        ArrayList<Song> songs = new ArrayList<Song>();
        try {
            // Define a list to store items in the list of uploaded videos.
            List<PlaylistItem> playlistItemList = new ArrayList<PlaylistItem>();

            // Retrieve the playlist of the channel's uploaded videos.
            YouTube.PlaylistItems.List playlistItemRequest =
                    youtube.playlistItems().list("id,contentDetails,snippet");
            playlistItemRequest.setPlaylistId(playlistId);


            playlistItemRequest.setFields(
                    "items(id,contentDetails/videoId,snippet/title,snippet/publishedAt),nextPageToken,pageInfo");

            String nextToken = "";

            // Call the API one or more times to retrieve all items in the
            // list. As long as the API response returns a nextPageToken,
            // there are still more items to retrieve.
            do {
                playlistItemRequest.setPageToken(nextToken);
                PlaylistItemListResponse playlistItemResult = playlistItemRequest.execute();

                playlistItemList.addAll(playlistItemResult.getItems());

                nextToken = playlistItemResult.getNextPageToken();
            } while (nextToken != null);


            Iterator<PlaylistItem> playlistEntries =  playlistItemList.iterator();
            Log.d("getPlaylistItems", playlistItemList.size()+"");

            List<String> videoIds = new ArrayList<String>();

            // Merge video IDs
            for (PlaylistItem playlistItem : playlistItemList) {
                activity.setFavoritPlayListItemId(playlistItem.getContentDetails().getVideoId(),
                        playlistItem.getId());

                videoIds.add(playlistItem.getContentDetails().getVideoId());
            }

            activity.printsetFavoritPlayListItemId();

            Joiner stringJoiner = Joiner.on(',');
            String videoId = stringJoiner.join(videoIds);

            // Call the YouTube Data API's youtube.videos.list method to
            // retrieve the resources that represent the specified videos.
            YouTube.Videos.List listVideosRequest = youtube.videos().list("snippet, recordingDetails, statistics").setId(videoId);
            VideoListResponse listResponse = listVideosRequest.execute();

            List<Video> videoList = listResponse.getItems();

            if (videoList != null) {
                for (Video video : videoList) {
                    Log.d("playlistVideo", " Video Id" + video.getId());
                    Log.d("playlistVideo", " Title: " + video.getSnippet().getTitle());
                    Log.d("playlistVideo", " Thumbnail: " + video.getSnippet().getThumbnails().getDefault().getUrl());
                    Log.d("playlistVideo", " Published date: " + video.getSnippet().getPublishedAt().toString());
                    Log.d("playlistVideo", " Views: " + String.valueOf(video.getStatistics().getViewCount()));
                    Log.d("getSearchResult", "\n-------------------------------------------------------------\n");

                    songs.add(new Song(video.getId(), null,
                            video.getSnippet().getThumbnails().getDefault().getUrl(),
                            video.getSnippet().getTitle(), video.getSnippet().getPublishedAt().toString(),
                            String.valueOf(video.getStatistics().getViewCount())));
                }
            }
        } catch (GoogleJsonResponseException e) {
            e.printStackTrace();
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());

        } catch (Throwable t) {
            t.printStackTrace();
        }
        return songs;
    }
}