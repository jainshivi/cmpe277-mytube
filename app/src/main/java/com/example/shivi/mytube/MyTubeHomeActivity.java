package com.example.shivi.mytube;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.Toast;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.example.shivi.mytube.adapter.FavoriteListAdapter;
import com.example.shivi.mytube.adapter.SearchListAdapter;
import com.example.shivi.mytube.model.Song;
import com.example.shivi.mytube.youtube.Config;
import com.example.shivi.mytube.youtube.PlaylistManager;
import com.example.shivi.mytube.youtube.Search;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.shivi.mytube.R.drawable.mytube;

public class MyTubeHomeActivity extends YouTubeBaseActivity
        implements YouTubePlayer.OnInitializedListener {

    private ListView favoriteListView;
    private ListView searchListView;
    private SearchListAdapter searchListAdapter;
    private FavoriteListAdapter favoriteListAdapter;
    private ArrayList<Song> selectedVideos = new ArrayList<Song>();
    ArrayList<Song> songSearchResults;
    private MyTubeHomeActivity currentActivity;

    private SearchView searchView;
    private String lastSearchQuery;

    private String playlistId = null;
    private Map<String, String> favoriteMap = new HashMap<String, String>();

    private GoogleAccountCredential credential;
    private static YouTube youtube;
    public static String PREF_ACCOUNT_NAME = "";
    static final String REQUEST_AUTHORIZATION_INTENT = "com.google.example.yt.RequestAuth";
    static final String REQUEST_AUTHORIZATION_INTENT_PARAM = "com.google.example.yt.RequestAuth.param";
    public static final int REQUEST_AUTHORIZATION = 3;

    String frag = "Search";

    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayer youTubePlayer;
    private YouTubePlayerFragment youTubePlayerFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_mytube_home);
        ArrayList<String> scopes = new ArrayList<String>();
        scopes.add(0, YouTubeScopes.YOUTUBE);
        scopes.add(1, YouTubeScopes.YOUTUBE_FORCE_SSL);
        scopes.add(2, YouTubeScopes.YOUTUBEPARTNER);

        credential = GoogleAccountCredential.usingOAuth2(this, scopes);
        credential.setSelectedAccountName(PREF_ACCOUNT_NAME);

        ActionBar actionBar = getActionBar();
        actionBar.setTitle("");
        actionBar.setIcon(mytube);

        initializeTabView();

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager()
                .findFragmentById(R.id.youtubeplayerfragment);
        youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY, this);

        this.toggleVideoView(false);
        LoadPreferences();
    }

    private void initializeTabView() {
        currentActivity = this;
        searchListView = (ListView) findViewById(R.id.searchList);
        favoriteListView = (ListView) findViewById(R.id.favoriteList);
        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        TabHost.TabSpec spec1 = tabHost.newTabSpec("tab_Favorite");
        spec1.setContent(R.id.tab_Search);
        spec1.setIndicator("Search");
        tabHost.addTab(spec1);

        TabHost.TabSpec spec2 = tabHost.newTabSpec("tab_Favorite");
        spec2.setContent(R.id.tab_Favorite);
        spec2.setIndicator("Favorite");
        tabHost.addTab(spec2);

        new PlaylistManager(this, credential).execute(this.getPlaylistId(), null, "List");

        final EditText searchText = (EditText) findViewById(R.id.searchEditText);
        ImageButton searchButton = (ImageButton) findViewById(R.id.searchImageButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchText.getText().toString().trim().equals("")) {
                    searchText.setError("Please enter a text to search!");
                } else {
                    searchText.setError(null);
                    performSearch(searchText.getText().toString());
                }
            }
        });
    }

    private void toggleVideoView(Boolean show) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (show) {
            ft.show(youTubePlayerFragment);
        } else {
            ft.hide(youTubePlayerFragment);
        }

        ft.commit();
    }

    public void playVideo(String songId, String fragment)
    {
        frag = fragment;
        youTubePlayer.loadVideo(songId);
        this.toggleVideoView(true);
        hideKeyboard();
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        this.youTubePlayer = youTubePlayer;
        if (!b) {
            //youTubePlayer.cueVideo("fhWaJi1Hsfo"); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    private void performSearch(String searchTerm) {
        if (searchTerm != null) {
            lastSearchQuery = searchTerm;
            Search search = new Search(this, credential);
            search.execute(searchTerm);
            hideKeyboard();
        }
    }

    private void hideKeyboard(){
        try  {
            InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            Log.e("MainActivity", "Error while hiding keyboard", e);
        }
    }

    public void renderFavoriteList(ArrayList<Song> songList) {
        favoriteListAdapter = new FavoriteListAdapter(this, songList, credential);
        favoriteListView.setAdapter(favoriteListAdapter);
        favoriteListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        favoriteListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            int nr = 0;

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                if (checked) {
                    nr++;
                    favoriteListAdapter.setNewSelection(position, checked);
                } else {
                    nr--;
                    favoriteListAdapter.removeSelection(position);
                }
                mode.setTitle(nr + " selected");
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                nr = 0;
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.contextual_menu, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_delete:
                        nr = 0;
                        List<String> itemsToRemove = favoriteListAdapter.getItemsToRemove();
                        for (String videoId : itemsToRemove) {
                            new PlaylistManager(currentActivity, credential).execute(currentActivity.getPlaylistId(), videoId, "Remove");
                        }

                        favoriteListAdapter.clearSelection();
                        mode.finish();
                }
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                favoriteListAdapter.clearSelection();
            }
        });

        favoriteListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                favoriteListView.setItemChecked(position, !favoriteListAdapter.isPositionChecked(position));
                return false;
            }
        });
    }

    public void renderSearchResults(ArrayList<Song> songList) {
        songSearchResults = songList;
        searchListAdapter = new SearchListAdapter(this, songList, credential);
        searchListView.setAdapter(searchListAdapter);
    }

    private void SavePreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("searchstring", this.lastSearchQuery);
        editor.putString("frag", this.frag);
        editor.commit();
    }

    private void LoadPreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String searchquery = sharedPreferences.getString("searchstring", null);
        String frag = sharedPreferences.getString("frag", null);
        if(searchquery != null && !(frag != null && frag.equals("Favorite"))) {

            final EditText searchText = (EditText) findViewById(R.id.searchEditText);
            searchText.setText(searchquery);
            performSearch(searchquery);

        }
        else if(frag != null && frag.equals("Favorite"))
        {
            TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
            tabHost.setCurrentTab(1);
        }
        editor.clear();
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        SavePreferences();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_AUTHORIZATION:
                if (resultCode == Activity.RESULT_OK) {
                    performSearch(lastSearchQuery);
                }
                break;

            case RECOVERY_REQUEST:
                // Retry initialization if user performed a recovery action
                youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY, this);
                break;
        }
    }

    // START - Methods to keep track of playlist ids and corresponding video ids

    public String getPlaylistId() {
        return this.playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public void setFavoritPlayListItemId(String videoId, String playListItemId) {
        favoriteMap.put(videoId, playListItemId);
    }

    public String getFavoritPlayListItemId(String videoId) {
        if(favoriteMap.containsKey(videoId)){
            return favoriteMap.get(videoId);
        }
        return null;
    }

    public void removeFavoritPlayListItemId(String videoId){
        favoriteMap.remove(videoId);
    }

    public void printsetFavoritPlayListItemId(){
        for(String videoId : favoriteMap.keySet())
            Log.d("keyset:", videoId + ":" + favoriteMap.get(videoId));
    }

    // END - Methods to keep track of playlist ids and corresponding video ids
}
