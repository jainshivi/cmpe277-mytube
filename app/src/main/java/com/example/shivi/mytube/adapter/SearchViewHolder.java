package com.example.shivi.mytube.adapter;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.example.shivi.mytube.model.Song;

public class SearchViewHolder {
    TextView noOfViewsView;
    TextView nameView;
    TextView publishedDateView;
    ImageView imageView;
    ToggleButton toggleButton;
    public Song song;
}