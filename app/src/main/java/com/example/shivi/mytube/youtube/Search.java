package com.example.shivi.mytube.youtube;

import android.os.AsyncTask;
import android.util.Log;

import com.example.shivi.mytube.MyTubeHomeActivity;
import com.example.shivi.mytube.model.Song;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchListResponse;
import com.google.api.services.youtube.model.SearchResult;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoListResponse;
import com.google.api.client.util.Joiner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Search extends AsyncTask<String, Void, ArrayList<Song>> {
    private static final String PROPERTIES_FILENAME = "youtube.properties";
    private static final String PREF_ACCOUNT_NAME = "MyTube";
    private static final long NUMBER_OF_VIDEOS_RETURNED = 25;

    private MyTubeHomeActivity activity;
    private GoogleAccountCredential credential;
    public YouTube youtube;

    public Search(MyTubeHomeActivity activity, GoogleAccountCredential credential)
    {
        this.activity = activity;
        this.credential = credential;
    }

    /**
     * Initialize a YouTube object to search for videos on YouTube. Then
     * display the name and thumbnail image of each video in the result set.
     * @param queryTerm
     * @return
     */

    public ArrayList<Song> getSearchResult(String queryTerm) {
        ArrayList<Song> songs = new ArrayList<Song>();

        try {
            // YouTube client
            YouTube youtube = new YouTube.Builder(new NetHttpTransport(), new JacksonFactory(), credential)
                    .setApplicationName("MyTube").build();

            // Define the API request for retrieving search results.
            YouTube.Search.List search = youtube.search().list("id,snippet");

            search.setKey(Config.YOUTUBE_API_KEY);
            search.setQ(queryTerm);

            // Restrict the search results to only include videos. See:
            // https://developers.google.com/youtube/v3/docs/search/list#type
            search.setType("video");

            // To increase efficiency, only retrieve the fields that the application uses.
            search.setFields("items(id/kind,id/videoId,snippet/title,snippet/thumbnails/default/url,snippet/publishedAt)");
            search.setMaxResults(NUMBER_OF_VIDEOS_RETURNED);

            // Call the API and print results.
            SearchListResponse searchResponse = search.execute();
            List<SearchResult> searchResultList = searchResponse.getItems();
            Log.d("Search", "" + searchResultList.size());
            List<String> videoIds = new ArrayList<String>();

            if (searchResultList != null) {

               // Merge video IDs
               for (SearchResult searchResult : searchResultList) {
                   videoIds.add(searchResult.getId().getVideoId());
               }
               Joiner stringJoiner = Joiner.on(',');
               String videoId = stringJoiner.join(videoIds);

                // Call the YouTube Data API's youtube.videos.list method to
                // retrieve the resources that represent the specified videos.
                YouTube.Videos.List listVideosRequest = youtube.videos().list("snippet, recordingDetails, statistics").setId(videoId);
                VideoListResponse listResponse = listVideosRequest.execute();

                List<Video> videoList = listResponse.getItems();

                if (videoList != null) {
                    for (Video video : videoList) {
                        Log.d("getSearchResult", " Video Id" + video.getId());
                        Log.d("getSearchResult", " Title: " + video.getSnippet().getTitle());
                        Log.d("getSearchResult", " Thumbnail: " + video.getSnippet().getThumbnails().getDefault().getUrl());
                        Log.d("getSearchResult", " Published date: " + video.getSnippet().getPublishedAt().toString());
                        Log.d("getSearchResult", " Views: " + String.valueOf(video.getStatistics().getViewCount()));

                        Log.d("getSearchResult", "\n-------------------------------------------------------------\n");

                        songs.add(new Song(video.getId(), null,
                                video.getSnippet().getThumbnails().getDefault().getUrl(),
                                video.getSnippet().getTitle(), video.getSnippet().getPublishedAt().toString(),
                                String.valueOf(video.getStatistics().getViewCount())));
                    }
                }
            }
        } catch (GoogleJsonResponseException e) {
            String s = e.getDetails().getMessage();
            System.err.println("There was a service error: " + e.getDetails().getCode() + " : "
                    + e.getDetails().getMessage());
        } catch (UserRecoverableAuthIOException e) {
            this.activity.startActivityForResult(e.getIntent(), this.activity.REQUEST_AUTHORIZATION);
        } catch (IOException e) {
            System.err.println("There was an IO error: " + e.getCause() + " : " + e.getMessage());
        } catch (Throwable t) {
            t.printStackTrace();
        }
        return songs;
    }


    @Override
    public ArrayList<Song> doInBackground(String... params) {
        Log.d("Search", "In doInBackground" + params[0]);
        return getSearchResult(params[0]);
    }

    @Override
    protected void onPostExecute(ArrayList<Song> songs) {
        activity.renderSearchResults(songs);
    }
}
