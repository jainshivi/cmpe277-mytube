package com.example.shivi.mytube.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.ToggleButton;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.squareup.picasso.Picasso;

import com.example.shivi.mytube.MyTubeHomeActivity;
import com.example.shivi.mytube.youtube.PlaylistManager;
import com.example.shivi.mytube.model.Song;
import com.example.shivi.mytube.R;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import java.util.List;

public class SearchListAdapter extends BaseAdapter{
    private MyTubeHomeActivity activity;
    private LayoutInflater inflater;
    private List<Song> songItems;
    private ImageButton favoriteButton;
    private GoogleAccountCredential credential;
    private PlaylistManager playlistManager;

    public SearchListAdapter(MyTubeHomeActivity activity, List<Song> songItems, GoogleAccountCredential credential) {
        this.activity = activity;
        this.songItems = songItems;
        this.credential = credential;
    }

    @Override
    public int getCount() {
        return songItems.size();
    }

    @Override
    public Object getItem(int position) {
        return songItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SearchViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_row, null);
            holder = new SearchViewHolder();
            holder.nameView = (TextView) convertView.findViewById(R.id.name);
            holder.noOfViewsView = (TextView) convertView.findViewById(R.id.numberOfViews);
            holder.publishedDateView = (TextView) convertView.findViewById(R.id.publishedDate);
            holder.imageView = (ImageView) convertView.findViewById(R.id.videothumbnail);
            convertView.setTag(holder);
        } else {
            holder = (SearchViewHolder) convertView.getTag();
        }

        final Song song = songItems.get(position);
        holder.song = song;
        holder.nameView.setText(song.getName());
        holder.noOfViewsView.setText("Views: " + String.valueOf(song.getNumberOfViews()));
        holder.publishedDateView.setText("Published date: " + song.getPublishedDate());

        // Set thumbnail view
        if (holder.imageView != null) {
            Picasso.with(activity)
                    .load(song.getThumbnailUrl())
                    .placeholder(R.drawable.default_placeholder)
                    .into(holder.imageView);
        }

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.playVideo(song.getId(), "Search");
            }
        });

        // Set favorite button events
        holder.toggleButton = (ToggleButton)convertView.findViewById(R.id.toggleButton) ;
        holder.toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                if(isChecked){
                    if(activity.getFavoritPlayListItemId(song.getId()) == null) {
                        Log.d("My app", " Add to favorite : " + song.getId());
                        new PlaylistManager(activity, credential).execute(activity.getPlaylistId(), song.getId(), "Insert");
                    }
                } else {
                    if(activity.getFavoritPlayListItemId(song.getId()) != null) {
                        Log.d("My app", " Remove from favorite : " + song.getId());
                        new PlaylistManager(activity, credential).execute(activity.getPlaylistId(), song.getId(), "Remove");
                    }
                }
            }
        }) ;

        return convertView;
    }
}
